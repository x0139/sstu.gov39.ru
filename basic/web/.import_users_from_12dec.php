<?php
/**
 * Created by PhpStorm.
 * User: o.artemov
 * Date: 04.04.2016
 * Time: 14:51
 */

echo "test<br>";
$mysqli_12dec = new Mysqli('localhost', 'root', 'XqaUbvdLi7LA3Z7ZUr6PTjH7r1JZplfR', '12dec');
$mysqli_12dec->set_charset('utf8');


$selectAllUsers = 'select * from 01_users';

//print_r($mysqli_12dec);

$arrUsers = [];

$result = $mysqli_12dec->query($selectAllUsers);

while ($row = $result->fetch_assoc()){
    $user = [];
    $user['name'] = $row['user_lastname'];
    $user['login'] = $row['user_login'];
    $user['password'] = $row['user_hpass'];
    $user['email'] = $row['user_mail'];
    $user['status'] = $row['user_status'];
    $arrUsers[] = $user;
}


$result->free();
$mysqli_12dec->close();

$mysqli_sstu = new Mysqli('localhost', 'root', 'XqaUbvdLi7LA3Z7ZUr6PTjH7r1JZplfR', 'g39-sstu');
$mysqli_sstu->set_charset('utf8');
$stmt = $mysqli_sstu->prepare('INSERT INTO user (username, password, name, date_register, status, email)
                                          VALUES (?, ?, ?, ?, ?, ?)');

foreach($arrUsers as $user){
    $username = $user['login'];
    $password = $user['password'];
    $name = $user['name'];
    $date_register = date('Y-m-d H:i:s');
    $status = ($user['status'] == 'on') ? 'active' : 'inactive';
    $email = $user['email'];

    $stmt->bind_param('ssssss', $username, $password, $name, $date_register, $status, $email);

    $stmt->execute();
}

//echo "<pre>";
//print_r($arrUsers);
//echo "</pre>";