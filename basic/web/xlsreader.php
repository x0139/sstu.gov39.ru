<?php

/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */
class Vote
{
    public $groupName;
    public $executive;
    public $position;
    public $fio;
    public $termOfOffice;
    public $questions;

    /**
     * Vote constructor.
     * @param $groupName
     * @param $muniName
     * @param $executive
     * @param $position
     * @param $fio
     * @param $termOfOffice
     * @param $questions
     */
    public function __construct($groupName, $executive, $position, $fio, $termOfOffice, $questions)
    {
        $this->groupName = $groupName;
        $this->executive = $executive;
        $this->position = $position;
        $this->fio = $fio;
        $this->termOfOffice = $termOfOffice;
        $this->questions = $questions;
    }


}

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

// section for my Excel spreadsheet reader

$objPHPExcel = PHPExcel_IOFactory::load('for_php_sql_all.xlsx');


$objWorksheet = $objPHPExcel->getActiveSheet();
// Get the highest row and column numbers referenced in the worksheet
$highestRow = $objWorksheet->getHighestRow(); // e.g. 10
$highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5

$votes = [];
for ($row = 1; $row <= $highestRow; $row++)
{
    $groupName = '';
    $executive = '';
    $position = '';
    $fio = '';
    $termOfOffice = '';
    $questions = [];
    for ($col = 0; $col < $highestColumnIndex; $col++)
    {
        switch ($col)
        {
            case 0:
                $groupName = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                break;
            case 1:
                $executive = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                break;
            case 2:
                $position = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                break;
            case 3:
                $fio = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                break;
            case 4:
                $termOfOffice = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                break;
        }
        if ($col >= 5)
        {
            $value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
            if ($value)
            {
                $questions[$col - 4] = $value;
            }
        }
    }
    $votes[] = new Vote($groupName, $executive, $position, $fio, $termOfOffice, $questions);
}

//echo "<pre>";
//print_r($votes);
//echo "</pre>";

/* Алгоритм работы:
1. Запускаем цикл по массиву с классами голосовалок.
2. Получаем $groupName
3. Лезем в b_vote_channel, получаем id группы голосований
4. составляем опрос - берем $executive, $position, $fio, $termOfOffice, записываем в b_vote
5. заполняем вопросы - берем id опроса, берем массив $question, и для каждого $question[$key] делаем вставку в b_vote_question
запоминаем id question в массив $arQuestionId.
6. обрабатываем $arQuestionId - добавляем записи в b_vote_answer
*/

$count = 1;
$mysqli = new Mysqli("7.7.70.212", "g39-vote", "g39-vote", "g39_wwwdbt");
$mysqli->set_charset("utf8");
$timestamp_x = date('Y-m-d H:m:s');
foreach ($votes as $vote)
{
    $groupName = $vote->groupName;
    $sql = "SELECT id from b_vote_channel WHERE symbolic_name = '$groupName'";
    $result = $mysqli->query($sql);
    $idVote = $result->fetch_assoc()['id'];
    $title = $vote->fio;
    $description = $vote->executive . ' (' . $vote->position . ', срок полномочий - ' . $vote->termOfOffice . ')';


    $sqlVote = "INSERT INTO b_vote (
                                channel_id,
                                c_sort,
                                active,
                                timestamp_x,
                                date_start,
                                date_end,
                                counter,
                                title,
                                description,
                                description_type,
                                unique_type,
                                keep_ip_sec,
                                delay,
                                delay_type,
                                notify,
                                author_id
                                )
                VALUES (
                      $idVote,
                      100,
                      'Y',
                      '$timestamp_x',
                      '2016-03-01',
                      '2016-04-30 23:59:59',
                      0,
                      '$title',
                      '$description',
                      'html',
                      12,
                      86400,
                      1,
                      'D',
                      'N',
                      1
                      )";
    $mysqli->query($sqlVote);
    $lastVoteId = $mysqli->insert_id;
    $csort = 100;

    foreach ($vote->questions as $key => $value)
    {
        $quest = '';
        switch ($key)
        {
            case 1:
                $quest = "Организация транспортного обслуживания в муниципальном образовании";
                break;
            case 2:
                $quest = "Качество автомобильных дорог в муниципальном образовании";
                break;
            case 3:
                $quest = "Организация теплоснабжения (снабжение населения топливом)";
                break;
            case 4:
                $quest = "Организация водоснабжения";
                break;
            case 5:
                $quest = "Организация электроснабжения";
                break;
            case 6:
                $quest = "Организация газоснабжения";
                break;
        }
        $sqlQuestion = "INSERT INTO b_vote_question (
                                          active,
                                          timestamp_x,
                                          vote_id,
                                          c_sort,
                                          counter,
                                          question,
                                          question_type,
                                          diagram,
                                          required,
                                          diagram_type
                                          )
                    VALUES (
                      'Y',
                      '$timestamp_x',
                      '$lastVoteId',
                      $csort,
                      0,
                      '$quest',
                      'html',
                      'Y',
                      'Y',
                      'histogram'
                    )";
        //echo $sqlQuestion;
        $mysqli->query($sqlQuestion);
        $csort += 100;
        $lastQuestionId = $mysqli->insert_id;
        for($i = 1; $i <= 5; $i++){
            $csortAnswer = $i * 100;
            $sqlQuestAnswer = "INSERT INTO b_vote_answer (
                                                        active,
                                                        timestamp_x,
                                                        question_id,
                                                        c_sort,
                                                        message,
                                                        counter,
                                                        field_type,
                                                        field_width,
                                                        field_height,
                                                        message_type)
                                            VALUES (
                                              'Y',
                                              '$timestamp_x',
                                              $lastQuestionId,
                                              $csortAnswer,
                                              $i,
                                              0,
                                              0,
                                              0,
                                              0,
                                              'html'
                                            )";
            $mysqli->query($sqlQuestAnswer);
            //echo $sqlQuestAnswer . "<br>";
        }
    }

    echo $lastVoteId;
    
    //die;
    //echo $count . ". " . $groupName . ": " . $idVote . "<br>";
}