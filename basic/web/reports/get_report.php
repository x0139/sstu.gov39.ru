<?php
session_start();


if ($_SERVER['REQUEST_METHOD'] != 'POST'){
    header('Location: /site/report');
    exit;
}
//echo "<pre>";
//print_r($_SERVER);
//die();

$dateStartRaw = $_POST['date_start'];
$dateEndRaw = $_POST['date_end'];

$dateStartRawArr = date_parse($dateStartRaw);
$dateEndRawArr = date_parse($dateEndRaw);

$date_start = ''; 
$date_end = '';

if(checkdate($dateStartRawArr['month'], $dateStartRawArr['day'], $dateStartRawArr['year'])
    && checkdate($dateEndRawArr['month'], $dateEndRawArr['day'], $dateEndRawArr['year'])){
    $date_start = $dateStartRaw;
    $date_end = $dateEndRaw;
} else{
    header('Location: /site/report');
    exit;
}

include_once("report_lib.php");
include_once("report_xls.php");
include_once("report_main.php")


?>