<?php

function setXlsData($sheet, $title, $eventResultsArr, $users, $date_start, $date_end)
{
//    echo "<pre>";
//    print_r($users);
//    die();
//    global $users;
    global $typeCategories;
    global $answers;
//    global $muniEventResults;
    global $types;

    $sheet->setTitle($title);

    $sheet->setCellValue("A1", "АНАЛИЗ ПОСТУПИВШИХ В ОРГАНЫ ИСПОЛНИТЕЛЬНОЙ ВЛАСТИ ОБРАЩЕНИЙ (ВОПРОСОВ) ГРАЖДАН, ОРГАНИЗАЦИЙ (ЮРИДИЧЕСКИХ) И ОБЩЕСТВЕННЫХ ОБЪЕДИНЕНИЙ, АДРЕСОВАННЫХ ГУБЕРНАТОРУ КАЛИНИНГРАДСКОЙ ОБЛАСТИ В РАЗРЕЗЕ МУНИЦИПАЛЬНЫХ ОБРАЗОВАНИЙ (ПО ТЕМАТИЧЕСКИМ РАЗДЕЛАМ) ЗА ПЕРИОД $date_start - $date_end");
    $sheet->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->mergeCells('A1:U1');
    $sheet->getStyle('A1:U2')->getAlignment()->setWrapText(true);
    $sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $sheet->getRowDimension(1)->setRowHeight(50);
    $sheet->getStyle('A1')->getFont()->setBold(true);

    $row = "A";
    $col = "2";
    $sheet->setCellValue($row . $col, "№ пп");
    $sheet->getStyle($row . $col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $row++;
    $sheet->setCellValue($row . $col, 'Содержание вопросов');
    $sheet->getStyle($row . $col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $sheet->getRowDimension(2)->setRowHeight(200);
    $sheet->getColumnDimension('B')->setWidth(100);

    $row++;
    $sheet->setCellValue($row . $col, 'Всего обращений');
    $sheet->getStyle($row . $col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $sheet->getStyle($row . $col)->getAlignment()->setTextRotation(90);
    $row++;

    $countMuni = 3; // позиция, с которой вставляются наименования органов
    foreach ($users as $user) {
        $sheet->setCellValue($row . $col, $user);
        $sheet->getStyle($row . $col)->getAlignment()->setTextRotation(90);
        $sheet->getStyle($row . $col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $row++;
        $countMuni++;
    }

    $col++;
    $row = 'A';

    for ($i = 1; $i <= $countMuni; $i++) {
        $sheet->setCellValue($row . $col, $i);
        $sheet->getStyle($row . $col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $row++;
    }

    $col++;
    $row = 'A';

    $itogoCol = $col;
    $itogoNumbersArr = [];

    $col++;

    $count = 1;
    $arrNumbers4Social = [];
    $arrNumbers4Economy = [];

// формируем основную часть отчета
    foreach ($typeCategories as $typeId => $categoryArr) {
        $row = 'A';
        $count4ItogoByType = $count;
        $sheet->setCellValue($row . $col, $count);
        $sheet->getStyle($row . $col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


        $row = 'B';
        // костыль № 1 для исключение типов из ИТОГО
        if ($typeId == 3 || $typeId == 5) {
            $count++;
        } else {
            $itogoNumbersArr[] = $count++;
        }
        $col4itogoByType = $col++;
        $arrNumbers = [];

        foreach ($categoryArr as $category) {
            $row = 'A';
            $sheet->setCellValue($row . $col, $count);
            $sheet->getStyle($row . $col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $row = 'B';
            $sheet->setCellValue($row . $col, $category)->getRowDimension($col)->setRowHeight(17);
            $col++;
//            if ($typeId == 2 ){
//                $arrNumbers4Social[] = $count++;
//            } elseif ($typeId == 4 ){
//                $arrNumbers4Economy[] = $count++;
//            } else {
            $arrNumbers[] = $count++;
//            }
        }

        $text = '';
        if ($typeId == 2 || $typeId == 4) {
            $tempNum = $arrNumbers[count($arrNumbers) - 1] + 1;
            $text = $types[$typeId] . " ($count4ItogoByType=" . implode("+", $arrNumbers) . "+$tempNum)";
        } else {
            $text = $types[$typeId] . " ($count4ItogoByType=" . implode("+", $arrNumbers) . ")";
        }
        $sheet->setCellValue($row . $col4itogoByType, $text)->getRowDimension($col4itogoByType)->setRowHeight(17);
        $sheet->getStyle($row . $col4itogoByType)->getFont()->setBold(true);
    }

    $textItogo = "ИТОГО в том числе: (=" . implode("+", $itogoNumbersArr) . ")";
    $sheet->setCellValue($row . $itogoCol, $textItogo)->getStyle($row . $itogoCol)->getFont()->setBold(true);
    $sheet->getRowDimension($itogoCol)->setRowHeight(17);

    $resultsCol = $col++;
    $resultsCount = $count++;
    $resultsNumberArr = [];

    foreach ($answers as $answer) {
        $row = "A";
        $resultsNumberArr[] = $count;
        $sheet->setCellValue($row . $col, $count++);
        $sheet->getStyle($row . $col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $row = "B";
        $sheet->setCellValue($row . $col, $answer);
        $sheet->getRowDimension($col++)->setRowHeight(17);
    }

    $row = "A";
    $sheet->setCellValue($row . $resultsCol, $resultsCount);
    $sheet->getStyle($row . $resultsCol)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $row = "B";
    $textItogoResult = "Результативность ($resultsCount=" . implode("+", $resultsNumberArr) . ")";
    $sheet->setCellValue($row . $resultsCol, $textItogoResult)->getStyle($row . $resultsCol)->getFont()->setBold(true);
    $sheet->getRowDimension($resultsCol)->setRowHeight(17);

    $row = 'C';
    $col = '4';
    $lastCol = 0;
    $lastRow = 0;

    $sum4SocialType = 0;
    $sum4SocialCol = 0;
    $sum4EconomyType = 0;
    $sum4EconomyCol = 0;

    foreach ($eventResultsArr as $arrayId => $eventResultArr) {
        
        $sumByUserCol = $col;
        $sumByUser = 0;
        $sumByUser4SocialCol = 0;
        $sumByUser4Social = 0;
        $sumByUser4EconomyCol = 0;
        $sumByUser4Economy = 0;
        $col++;

        $answersArr = [];
        foreach ($eventResultArr as $typeId => $typeArr) {
            $sumByTypeCol = $col;
            if ($typeId == 2){
                $sum4SocialCol = $col;
                $sumByUser4SocialCol = $col;
            } elseif ($typeId == 4){
                $sum4EconomyCol = $col;
                $sumByUser4EconomyCol = $col;
            }
            $col++;
            $sumByType = 0;
            foreach ($typeArr as $catId => $categoryArr) {
                foreach ($categoryArr as $answerId => $answerCount) {
                    if (isset($answersArr[$answerId])) {
                        $answersArr[$answerId] += $answerCount;
                    } else {
                        $answersArr[$answerId] = 0;
                    }
                }
                $sum = array_sum($categoryArr);
                $sumByType += $sum;
                $sheet->setCellValue($row . $col, $sum);
                $col++;
            }
            $sheet->setCellValue($row . $sumByTypeCol, $sumByType);
            $sumByUser += $sumByType;
            if (($typeId == 2 || $typeId == 3)){
                $sum4SocialType += $sumByType;
                $sumByUser4Social += $sumByType;
            } 
            if (($typeId == 4 || $typeId == 5)){
                $sum4EconomyType += $sumByType;
                $sumByUser4Economy +=$sumByType;
            }
            if ($typeId == 3 && $arrayId == 0){
                $sheet->setCellValue($row . $sum4SocialCol, $sum4SocialType);
            } elseif ($typeId == 5 && $arrayId == 0){
                $sheet->setCellValue($row . $sum4EconomyCol, $sum4EconomyType);
            }

            if ($typeId == 3 && $arrayId > 0){
                $sheet->setCellValue($row . $sumByUser4SocialCol, $sumByUser4Social);
            } elseif ($typeId == 5 && $arrayId > 0){
                $sheet->setCellValue($row . $sumByUser4EconomyCol, $sumByUser4Economy);
            }
        }
        
        $sheet->setCellValue($row . $sumByUserCol, $sumByUser);

        $sheet->setCellValue($row . $col++, array_sum($answersArr));

        foreach ($answersArr as $answerCount) {
            $sheet->setCellValue($row . $col++, $answerCount);
        }

        $lastCol = --$col;
        $lastRow = $row;
        $col = 4;
        $row++;
    }

//    echo "<pre>";
//    print_r($arrNumbers4Social);
//    print_r($arrNumbers4Economy);

    /*
    foreach ($typeCategories as $typeId => $categoryArr) {
        $row = 'A';
        $count4ItogoByType = $count;
        $sheet->setCellValue($row . $col, $count);
        $sheet->getStyle($row . $col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $row = 'B';

        // костыль № 1 для исключение типов из ИТОГО
        if ($typeId == 3 || $typeId == 5){
            $count++;
        } else {
            $itogoNumbersArr[] = $count++;
        }
        
        $col4itogoByType = $col++;
        $arrNumbers = [];
        foreach ($categoryArr as $category) {
            $row = 'A';
            $sheet->setCellValue($row . $col, $count);
            $sheet->getStyle($row . $col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $row = 'B';
            $sheet->setCellValue($row . $col, $category)->getRowDimension($col)->setRowHeight(17);
            $col++;
            $arrNumbers[] = $count++;
        }
        $text = $types[$typeId] . " ($count4ItogoByType=" . implode("+", $arrNumbers) . ")";
        $sheet->setCellValue($row . $col4itogoByType, $text)->getRowDimension($col4itogoByType)->setRowHeight(17);
        $sheet->getStyle($row . $col4itogoByType)->getFont()->setBold(true);
    }

    $textItogo = "ИТОГО в том числе: (=" . implode("+", $itogoNumbersArr) . ")";
    $sheet->setCellValue($row . $itogoCol, $textItogo)->getStyle($row . $itogoCol)->getFont()->setBold(true);
    $sheet->getRowDimension($itogoCol)->setRowHeight(17);

    $resultsCol = $col++;
    $resultsCount = $count++;
    $resultsNumberArr = [];

    foreach ($answers as $answer) {
        $row = "A";
        $resultsNumberArr[] = $count;
        $sheet->setCellValue($row . $col, $count++);
        $sheet->getStyle($row . $col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $row = "B";
        $sheet->setCellValue($row . $col, $answer);
        $sheet->getRowDimension($col++)->setRowHeight(17);
    }

    $row = "A";
    $sheet->setCellValue($row . $resultsCol, $resultsCount);
    $sheet->getStyle($row . $resultsCol)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $row = "B";
    $textItogoResult = "Результативность ($resultsCount=" . implode("+", $resultsNumberArr) . ")";
    $sheet->setCellValue($row . $resultsCol, $textItogoResult)->getStyle($row . $resultsCol)->getFont()->setBold(true);
    $sheet->getRowDimension($resultsCol)->setRowHeight(17);

    $row = 'C';
    $col = '4';
    $lastCol = 0;
    $lastRow = 0;

    foreach ($eventResultsArr as $eventResultArr) {
        $sumByUserCol = $col++;
        $sumByUser = 0;
        $answersArr = [];
        foreach ($eventResultArr as $typeArr) {
            $sumByTypeCol = $col++;
            $sumByType = 0;
            foreach ($typeArr as $typeId => $categoryArr) {
                foreach ($categoryArr as $answerId => $answerCount) {
                    if (isset($answersArr[$answerId])) {
                        $answersArr[$answerId] += $answerCount;
                    } else {
                        $answersArr[$answerId] = 0;
                    }
                }
                $sum = array_sum($categoryArr);
                $sumByType += $sum;
                $sheet->setCellValue($row . $col, $sum);
                $col++;
            }
            $sheet->setCellValue($row . $sumByTypeCol, $sumByType);
            $sumByUser += $sumByType;
        }
        $sheet->setCellValue($row . $sumByUserCol, $sumByUser);

        $sheet->setCellValue($row . $col++, array_sum($answersArr));

        foreach ($answersArr as $answerCount) {
            $sheet->setCellValue($row . $col++, $answerCount);
        }

        $lastCol = --$col;
        $lastRow = $row;
        $col = 4;
        $row++;
    }

    $sheet->getStyle('A1:' . $lastRow . $lastCol)->getAlignment()->setWrapText(true);

    $styleArray = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
    );

    $sheet->getStyle('A2:' . $lastRow . $lastCol)->applyFromArray($styleArray);
    */
}
    