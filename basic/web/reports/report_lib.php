<?php

//$date_start = '2016-01-01';
//$date_end = '2016-12-31';

//echo $date_start;
//echo "<br>";
//echo $date_end;
//die();

$dbProperties = [
    'host' => 'localhost',
    'username' => 'g39-sstu',
    'password' => 'rsUCZrudaGMnXPFoZE1jPz33Di5SmHRu',
    'db' => 'g39-sstu',
    'charset' => 'utf8',
];

$mysqli = new Mysqli($dbProperties['host'], $dbProperties['username'], $dbProperties['password'], $dbProperties['db']); //
$mysqli->set_charset($dbProperties['charset']);

function getEvents($date_start, $date_end)
{
    global $mysqli;

    $user_id = $_SESSION['user']['id'];
    $user_group_id = $_SESSION['user']['group_id'];

    if ($user_group_id == 0) {
        $selAllEvents = "SELECT id, user_id FROM visit_event WHERE status ='active' 
        AND date_visit_start >= '$date_start' AND date_visit_end < '$date_end' 
        AND user_id in (select id from user where group_id > 0)";
    } else {
        $selAllEvents = "SELECT id, user_id FROM visit_event WHERE status ='active' 
        AND date_visit_start >= '$date_start' AND date_visit_end < '$date_end' 
        AND user_id = '$user_id'";
//        echo $selAllEvents;
//        exit();
    }

//    echo $selAllEvents;
//    die();
    $result = $mysqli->query($selAllEvents);
    $arr = [];
    while ($row = $result->fetch_assoc()) {
        $arr[$row['id']] = $row['user_id'];
    }
    return $arr;
}

$events = getEvents($date_start, $date_end);
$eventsStr = implode(",", array_keys($events));
if (!$eventsStr) {
    echo "<p>Данных не найдено!</p>";
    echo "<p><a href='/site/report/'>Вернуться назад</a></p>";
    die();
}

//echo "<pre>";
//print_r($eventsStr);
//die();

function getRegions()
{
    global $mysqli;
    $selAllRegions = "SELECT id, region_title FROM sp_region WHERE status ='active' ORDER BY sort_order, region_title";
    $result = $mysqli->query($selAllRegions);
    $arr = [];
    while ($row = $result->fetch_assoc()) {
        $arr[$row['id']] = $row['region_title'];
    }
    return $arr;
}

$regions = getRegions();
//echo "<pre>";
//print_r($regions);
//die();

function getTypes()
{
    global $mysqli;
    $selAllTypes = "SELECT id, type_title FROM sp_type WHERE status = 'active'";
    $result = $mysqli->query($selAllTypes);
    $arr = [];
    while ($row = $result->fetch_assoc()) {
        $arr[$row['id']] = $row['type_title'];
    }
    return $arr;
}

$types = getTypes();

function getCategories()
{
    global $mysqli;
    $selAllCategories = "SELECT	category_title, id, type_id FROM sp_category where status = 'active'";
    $result = $mysqli->query($selAllCategories);
    $arr = [];
    while ($row = $result->fetch_assoc()) {
        $arr[$row['id']]['type_id'] = $row['type_id'];
        $arr[$row['id']]['category_title'] = $row['category_title'];
    }
    return $arr;
}

$categories = getCategories();


function getTypeCategories()
{
    global $mysqli;
    $selAllCategories = "SELECT	category_title, id, type_id FROM sp_category where status = 'active'";
    $result = $mysqli->query($selAllCategories);
    $arr = [];
    while ($row = $result->fetch_assoc()) {
        $arr[$row['type_id']][$row['id']] = $row['category_title'];
    }
    return $arr;
}

$typeCategories = getTypeCategories();

function getAnswers()
{
    global $mysqli;
    $selAllAnswers = "SELECT id, answer_title FROM sp_answer where status='active'";
    $result = $mysqli->query($selAllAnswers);
    $arr = [];
    while ($row = $result->fetch_assoc()) {
        $arr[$row['id']] = $row['answer_title'];
    }
    return $arr;
}

$answers = getAnswers();

function getUsers()
{
    global $mysqli;
    $selAllUsers = "SELECT id, group_id, name FROM user WHERE status = 'active' and group_id > 0 ORDER BY sort_order, name";
    $result = $mysqli->query($selAllUsers);
    $arr = [];
    while ($row = $result->fetch_assoc()) {
        $arr[$row['id']]['group_id'] = $row['group_id'];
        $arr[$row['id']]['name'] = $row['name'];
    }
    return $arr;
}

$users = getUsers();

$usersOiv = [];
$usersMuni = [];
foreach ($users as $user) {
    if ($user['group_id'] == 1) {
        $usersOiv[] = $user['name'];
    } elseif ($user['group_id'] == 2) {
        $usersMuni[] = $user['name'];
    }
}

function getUserGroups()
{
    global $mysqli;
    $selAllUsers = "SELECT id, title FROM user_group WHERE status = 'active'";
    $result = $mysqli->query($selAllUsers);
    $arr = [];
    while ($row = $result->fetch_assoc()) {
        $arr[$row['id']] = $row['title'];
    }
    return $arr;
}

$userGroups = getUserGroups();

function getVisitEventResults($events)
{
    global $mysqli;
    $selAll = "SELECT id, event_id, nrk, region_id, category_id, answer_id FROM visit_event_result WHERE event_id in ($events)";
    $result = $mysqli->query($selAll);
    $arr = [];
    while ($row = $result->fetch_assoc()) {
        $arr[$row['id']]['event_id'] = $row['event_id'];
        $arr[$row['id']]['nrk'] = $row['nrk'];
        $arr[$row['id']]['region_id'] = $row['region_id'];
        $arr[$row['id']]['category_id'] = $row['category_id'];
        $arr[$row['id']]['answer_id'] = $row['answer_id'];

    }
    return $arr;
}

$visitEventResults = getVisitEventResults($eventsStr);

$oivEventResults = [];
$muniEventResults = [];
$oivAll = [];
$muniAll = [];

// инициализация массива данных для отчета
foreach ($users as $userId => $user) {
    foreach ($typeCategories as $typeId => $categoryArr) {
        foreach ($categoryArr as $categoryId => $categoryName) {
            foreach ($answers as $answerId => $answer) {
                if ($user['group_id'] == 1) {
                    $oivAll[$typeId][$categoryId][$answerId] = 0;
                    $oivEventResults[$userId][$typeId][$categoryId][$answerId] = 0;
                } elseif ($user['group_id'] == 2) {
                    $muniAll[$typeId][$categoryId][$answerId] = 0;
                    $muniEventResults[$userId][$typeId][$categoryId][$answerId] = 0;
                }
            }
        }
    }
}

foreach ($visitEventResults as $quest_key => $visit) {
    $userId = $events[$visit['event_id']];
    $groupId = $users[$events[$visit['event_id']]]['group_id'];
//    echo "<pre>";
//    print_r($groupId);
//    echo "<hr></pre>";
//    if (!$groupId){
//        echo "<pre>";
//        print_r($users);
//        print_r($visit);
//        print_r($categories);
//        die();
//    }
    $typeId = $categories[$visit['category_id']]['type_id'];
    // fix for empty answers in visits
    if (!$typeId) {
        continue;
//        echo "<pre>";
//        print_r($visit);
//        print_r($categories);
//        die();
    }

    $categoryId = $visit['category_id'];
    $answerId = $visit['answer_id'];
    if ($groupId == 1) {
        $oivAll[$typeId][$categoryId][$answerId]++;
        $oivEventResults[$userId][$typeId][$categoryId][$answerId]++;
    } elseif ($groupId == 2) {
        $muniAll[$typeId][$categoryId][$answerId]++;
        $muniEventResults[$userId][$typeId][$categoryId][$answerId]++;
    }
}
//die();
// формируем конечные массивы
array_unshift($oivEventResults, $oivAll);
array_unshift($muniEventResults, $muniAll);

// формируем регион

$regionAll = [];
$regionEventResults = [];

foreach ($regions as $regionId => $regionName) {
    foreach ($typeCategories as $typeId => $categoryArr) {
        foreach ($categoryArr as $categoryId => $categoryName) {
            foreach ($answers as $answerId => $answer) {
                $regionAll[$typeId][$categoryId][$answerId] = 0;
                $regionEventResults[$regionId][$typeId][$categoryId][$answerId] = 0;
            }
        }
    }
}


foreach ($visitEventResults as $quest_key => $visit) {
    $regionId = $visit['region_id'];
//    $groupId = $users[$events[$visit['event_id']]]['group_id'];
    $typeId = $categories[$visit['category_id']]['type_id'];

    // fix for empty answers in visits
    if (!$typeId) {
        continue;
    }

    $categoryId = $visit['category_id'];
    $answerId = $visit['answer_id'];
    $regionAll[$typeId][$categoryId][$answerId]++;
    $regionEventResults[$regionId][$typeId][$categoryId][$answerId]++;
}

array_unshift($regionEventResults, $regionAll);

//echo "<pre>";
//print_r($regionEventResults);
//die();