<?php
// начало формирования Excel-отчета
require_once("../Classes/PHPExcel.php");
require_once('../Classes/PHPExcel/Writer/Excel5.php');

$xls = new PHPExcel();
$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet();
$title = 'ОМСУ';
setXlsData($sheet, $title, $muniEventResults, $usersMuni, $date_start, $date_end);

$sheet = $xls->createSheet();
$title = 'ОИВ';
setXlsData($sheet, $title, $oivEventResults, $usersOiv, $date_start, $date_end);

$sheet = $xls->createSheet();
$title = 'Регион';
setXlsData($sheet, $title, $regionEventResults, $regions, $date_start, $date_end);


// Выводим HTTP-заголовки
header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=report_$date_start-$date_end.xls");

// Выводим содержимое файла
$objWriter = new PHPExcel_Writer_Excel5($xls);
$objWriter->save('php://output');