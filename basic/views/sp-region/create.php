<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SpRegion */

$this->title = 'Create Sp Region';
$this->params['breadcrumbs'][] = ['label' => 'Sp Regions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sp-region-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
