<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SpRegion */

$this->title = 'Update Sp Region: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sp Regions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sp-region-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
