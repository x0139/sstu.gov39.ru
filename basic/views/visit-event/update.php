<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VisitEvent */

$this->title = 'Редактирование записи о приеме (id #' . $model->id . ')';
$this->params['breadcrumbs'][] = ['label' => 'Календарь приема', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="visit-event-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsVisitEventResult' => $modelsVisitEventResult,
    ]) ?>

    <p class="button-delete-date">
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


</div>
