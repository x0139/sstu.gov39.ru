<?php

use app\models\SpAnswer;
use app\models\SpCategory;
use app\models\SpRegion;
use app\models\SpType;
use yii\helpers\Html;
use yii\helpers\Url;
//use yii\widgets\ActiveForm;
use kartik\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseHtml;

/* @var $this yii\web\View */
/* @var $model app\models\VisitEvent */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="visit-event-form">

        <?php $form = ActiveForm::begin([
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
            //'id' => $model->formName(),
            'id' => 'dynamic-form',
            'enableAjaxValidation' => true,
            'validationUrl' => Url::toRoute('visit-event/validation')
        ]);
        ?>

        <div class="form-group kv-fieldset-inline">
            <?= Html::activeLabel($model, 'date_visit_start', [
                'label' => 'Начало приема',
                'class' => 'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-2">
                <?= $form->field($model, 'date_visit_start', [
                    'showLabels' => false
                ])->textInput(['placeholder' => 'Дата']); ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'startHour', [
                    'showLabels' => false
                ])->textInput(['placeholder' => 'Час']); ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'startMinute', [
                    'showLabels' => false
                ])->textInput(['placeholder' => 'Минута']); ?>
            </div>
        </div>
        <div class="form-group kv-fieldset-inline">
            <?= Html::activeLabel($model, 'date_visit_end', [
                'label' => 'Окончание приема',
                'class' => 'col-sm-2 control-label'
            ]) ?>
            <div class="col-sm-2">
                <?= $form->field($model, 'date_visit_end', [
                    'showLabels' => false
                ])->textInput(['placeholder' => 'Дата']); ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'endHour', [
                    'showLabels' => false
                ])->textInput(['placeholder' => 'Час']); ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'endMinute', [
                    'showLabels' => false
                ])->textInput(['placeholder' => 'Минута']); ?>
            </div>
        </div>

        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Результаты приема</h4></div>
                <div class="panel-body">
                    <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                        'widgetBody' => '.container-items', // required: css class selector
                        'widgetItem' => '.item', // required: css class
                        'limit' => 999, // the maximum times, an element can be cloned (default 999)
                        'min' => 1, // 0 or 1 (default 1)
                        'insertButton' => '.add-item', // css class
                        'deleteButton' => '.remove-item', // css class
//                    'model' => $model,
                        'model' => $modelsVisitEventResult[0],
                        'formId' => 'dynamic-form',
                        'formFields' => [
                            'nrk',
                            'region_id',
                            'category_id',
                            'answer_id',
                            'status',
                        ],
                    ]); ?>
                    <div class="container-items"><!-- widgetContainer -->
                        <?php foreach ($modelsVisitEventResult as $i => $modelVisitEventResult): ?>
                            <div class="item panel panel-default"><!-- widgetBody -->
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Обращение гражданина</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item btn btn-success btn-xs"><i
                                                class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item btn btn-danger btn-xs"><i
                                                class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                    // necessary for update action.
                                    if (!$modelVisitEventResult->isNewRecord) {
                                        echo Html::activeHiddenInput($modelVisitEventResult, "[{$i}]id");
                                    }
                                    ?>

                                    <div class="row">
                                        <div class="col-md-10">
                                            <?= $form->field($modelVisitEventResult, "[{$i}]nrk")->textInput(['maxlength' => true])->label("№ р/к") ?>
                                        </div>
                                        <div class="col-md-10">
                                            <?= $form->field($modelVisitEventResult, "[{$i}]region_id")->dropDownList(
                                                ArrayHelper::map(SpRegion::find()->all(), 'id', 'region_title'),
                                                ['prompt' => 'Выберите регион']
                                            )->label("Регион")
                                            ?>
                                        </div>
                                        <div class="col-md-10">
                                                <?
                                                $onChange = '
                                            var myId = $(this).attr(\'id\').match(/\d+/);
                                            $.post("/sp-category/lists?id="+$(this).val(), function(data) {
                                                        $("select#visiteventresult-" + myId + "-category_id").html(data);
                                                        });';
                                                echo $form->field($modelVisitEventResult, "[{$i}]type_id")->dropDownList(
                                                    ArrayHelper::map(SpType::find()->all(), 'id', 'type_title'),
                                                    [
                                                        'prompt' => 'Выберите тип',
                                                        'onchange' => "$onChange",
                                                    ])->label('Тип вопроса'); ?>
                                        </div>
                                        <div class="col-md-10">
                                            <?= $form->field($modelVisitEventResult, "[{$i}]category_id")->dropDownList(
                                                ArrayHelper::map(SpCategory::find()->all(), 'id', 'category_title'),
                                                ['prompt' => 'Выберите категорию']
                                            )->label('Категория вопроса') ?>
                                        </div>
                                        <div class="col-md-10">
                                            <?= $form->field($modelVisitEventResult, "[{$i}]answer_id")->dropDownList(
                                                ArrayHelper::map(SpAnswer::find()->all(), 'id', 'answer_title'),
                                                ['prompt' => 'Выберите результат']
                                            )->label('Результат') ?>
                                        </div>
                                    </div><!-- .row -->
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php DynamicFormWidget::end(); ?>
                </div>
            </div>
        </div>

        <div class="form-group kv-fieldset-submit">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>


    </div>

<?php ActiveForm::end(); ?>