<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\VisitEvent */

$this->title = 'Создание записи о приеме';
$this->params['breadcrumbs'][] = ['label' => 'Сведения о графике приема', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visit-event-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsVisitEventResult' => $modelsVisitEventResult,
    ]) ?>

</div>
