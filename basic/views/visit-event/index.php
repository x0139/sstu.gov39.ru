<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VisitEventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Календарь приема';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visit-event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Create Visit Event', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?php
    Modal::begin([
        'header' => '<h4>График приема</h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>

    <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
        'events'=> $events,
        'options' => [
            'lang' => 'ru',
            //... more options to be defined here!
        ],
    ));
    ?>

<!--    --><?//= GridView::widget([
//        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
//        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'id',
//            'user_id',
//            'date_visit_start',
//            'date_visit_end',
//            'status',
//            'date_zap',
//
//            ['class' => 'yii\grid\ActionColumn'],
//        ],
//    ]); ?>

</div>