<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\VisitEventResult */

$this->title = 'Create Visit Event Result';
$this->params['breadcrumbs'][] = ['label' => 'Visit Event Results', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visit-event-result-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
