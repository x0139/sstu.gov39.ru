<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VisitEventResultSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="visit-event-result-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'event_id') ?>

    <?= $form->field($model, 'nrk') ?>

    <?= $form->field($model, 'region_id') ?>

    <?= $form->field($model, 'category_id') ?>

    <?php // echo $form->field($model, 'answer_id') ?>

    <?php // echo $form->field($model, 'date_zap') ?>

<!--    --><?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
