<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\User;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>

<?
// check if user admin - add the Users tab
$userRole = 0;
if (!Yii::$app->user->isGuest) {
    $userId = Yii::$app->user->id;
    $userRole = Yii::$app->user->identity->findIdentity($userId)->admin;
}
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'sstu.gov39.ru',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    if ($userRole === User::ROLE_USER) {
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'На главную', 'url' => ['/site/index']],
                ['label' => 'Личный прием', 'url' => ['/visit-event/'], 'active' => Yii::$app->request->getUrl() == Url::toRoute(['/visit-event/'])],
                //['label' => 'Пользователи', 'url' => ['/user/'], 'active' => Yii::$app->request->getUrl() == Url::toRoute(['/user/'])],
                [
                    'label' => 'Администрирование',
                    'items' => [
                        ['label' => 'Отчет', 'url' => ['/site/report/'], 'active' => Yii::$app->request->getUrl() == Url::toRoute(['/site/report/'])],
                    ],
                ],
                //['label' => 'О сайте', 'url' => ['/site/about']],
                ['label' => 'Контакты', 'url' => ['/site/contact']],
                Yii::$app->user->isGuest ? (
                ['label' => 'Войти', 'url' => ['/site/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Выйти (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);
    } else {
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'На главную', 'url' => ['/site/index']],
                ['label' => 'Личный прием', 'url' => ['/visit-event/'], 'active' => Yii::$app->request->getUrl() == Url::toRoute(['/visit-event/'])],
                [
                    'label' => 'Администрирование',
                    'items' => [
                        ['label' => 'Отчет', 'url' => ['/site/report/'], 'active' => Yii::$app->request->getUrl() == Url::toRoute(['/site/report/'])],
                        ['label' => 'График приема граждан руководетями ОИВ', 'url' => ['/site/schedule/'], 'active' => Yii::$app->request->getUrl() == Url::toRoute(['/site/schedule/'])],
                        '<li class="divider"></li>',
                        '<li class="dropdown-header">Справочники</li>',
                        ['label' => 'Пользователи', 'url' => ['/user/'], 'active' => Yii::$app->request->getUrl() == Url::toRoute(['/user/'])],
                        ['label' => 'Все сведения таблицей', 'url' => ['/visit-event-result/'], 'active' => Yii::$app->request->getUrl() == Url::toRoute(['/visit-event-result/'])],
                        ['label' => 'Типы категорий', 'url' => ['/sp-type/'], 'active' => Yii::$app->request->getUrl() == Url::toRoute(['/sp-type/'])],
                        ['label' => 'Категории', 'url' => ['/sp-category/'], 'active' => Yii::$app->request->getUrl() == Url::toRoute(['/sp-category/'])],
                        ['label' => 'Регионы', 'url' => ['/sp-region/'], 'active' => Yii::$app->request->getUrl() == Url::toRoute(['/sp-region/'])],
                        ['label' => 'Результаты рассмотрений', 'url' => ['/sp-answer/'], 'active' => Yii::$app->request->getUrl() == Url::toRoute(['/sp-answer/'])],
                        ['label' => 'Группы пользователей', 'url' => ['/user-group/'], 'active' => Yii::$app->request->getUrl() == Url::toRoute(['/user-group/'])],
                    ],
                ],
                //['label' => 'О сайте', 'url' => ['/site/about']],
                ['label' => 'Контакты', 'url' => ['/site/contact']],
                Yii::$app->user->isGuest ? (
                ['label' => 'Войти', 'url' => ['/site/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Выйти (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);
    }

    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Правительство Калининградской области, <?= date('Y') ?></p>

        <p class="pull-right"><? //= date('Y') ?></p>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
