<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'globalSearch')->label('Поиск по пользователям') ?>

    <!--    --><? //= $form->field($model, 'username') ?>
    <!---->
    <!--    --><? //= $form->field($model, 'password') ?>
    <!---->
    <!--    --><? //= $form->field($model, 'auth_key') ?>
    <!---->
    <!--    --><? //= $form->field($model, 'access_token') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'date_register') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?//= Html::resetButton('Сбросить', ['class' => 'btn btn-default',]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
