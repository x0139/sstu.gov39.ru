<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Добавить пользователя', ['value' => Url::to('/user/create'), 'class' => 'btn btn-success', 'id' => 'modalButton']) ?>
    </p>

    <?php
    Modal::begin([
        'header' => '<h4>Пользователи</h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>

    <?php Pjax::begin(['id'=>'usersGrid']); ?>
    <?= GridView::widget(['dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'rowOptions' => function ($model)
        {
            if ($model->status == 'inactive')
            {
                return ['class' => 'danger'];
            } else {
                return ['class' => 'success'];
            }
        },
        'columns' => [['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            'password',
            //'auth_key',
            //'access_token',
            'name',
            'date_register',
            'email',
            'status',
            'admin',
            //'group_id',
            //'userGroup.title',
            [
                'attribute' => 'group_id',
                'value' => 'userGroup.title',
            ],
            'sort_order',
            ['class' => 'yii\grid\ActionColumn'],],]); ?>
    <?php Pjax::end(); ?>
</div>
