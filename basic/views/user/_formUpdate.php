<?php

use app\models\UserGroup;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="user-form">

        <?php $form = ActiveForm::begin([
            'id' => $model->formName(),
            'enableAjaxValidation' => true,
            'validationUrl' => Url::toRoute('user/validation-update?id=' . $model->id)
        ]); ?>

        <?//= $form->field($model, 'username')->textInput(['maxlength' => true])->label('Имя пользователя') ?>

        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true])->label('Пароль') ?>

        <!--    --><? //= $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>

        <!--    --><? //= $form->field($model, 'access_token')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Название ОИВ') ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label('E-mail') ?>

        <?= $form->field($model, 'group_id')->dropDownList(
            ArrayHelper::map(UserGroup::find()->all(), 'id', 'title'),
            ['prompt' => 'Выберите группу']
        ) ?>

        <!--    --><? //= $form->field($model, 'date_register')->textInput() ?>

        <?= $form->field($model, 'status')->dropDownList(['active' => 'Активный', 'inactive' => 'Деактивиров',], ['prompt' => 'Выберите статус'])->label('Статус') ?>

        <?= $form->field($model, 'admin')->dropDownList(['1' => 'Админ', '0' => 'Пользователь',], ['prompt' => 'Выберите роль'])->label('Роль пользователя') ?>

        <?= $form->field($model, 'sort_order')->textInput(['maxlength' => true])->label('Порядок сортировки') ?>
        
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php $script = <<< JS

$('form#{$model->formName()}').on('beforeSubmit', function(e)
{
    var \$form = $(this);
    $.post(
        \$form.attr("action"), //serialize Yii2 form
        \$form.serialize()
    )
        .done(function(result) {

        if(result == 1)
        {
            //$(\$form).trigger("reset");
            $(document).find('#modal').modal('hide');
            $.pjax.reload({container:'#usersGrid'});
        } else
        {
            $("#message").html(result);
        }
        }).fail(function()
        {
            console.log("server error");
        });
        return false;
});

JS;

$this->registerJs($script);
?>