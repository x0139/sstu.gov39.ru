<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>В случае возникновения проблем при работе с сайтом, обращаться:</p>
    <ol>
        <li>Административные вопросы - Сергей Сергеевич Афанасьев, телефон 8(4012)599-174, email: <a href="mailto:S.Afanasev@gov39.ru">S.Afanasev@gov39.ru</a></li>
        <li>Технические вопросы - Олег Викторович Артемов, телефон 8(4012)599-971, email: <a href="mailto:o.artemov@gov39.ru">o.artemov@gov39.ru</a></li>
    </ol>
</div>
