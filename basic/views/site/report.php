<?php
$date_start = date("Y-m-01");
$date_end = date("Y-m-d");

?>

<h1>
    Формирование статистического отчета
</h1>

<p>Введите диапазон дат формирования отчета:</p>
<form method="post" action="/reports/get_report.php" class="form-horizontal">
    <div class="col-xs-4">
        <label for="date_start">Дата начала</label>
        <input type="date" name="date_start" value="<?= $date_start; ?>" class="form-control" id="date_start">
    </div>
    <div class="col-xs-4">
        <label for="date_end">Дата окончания</label>
        <input type="date" name="date_end" value="<?= $date_end; ?>" class="form-control" id="date_end">
    </div>
    <div class="col-xs-4">
        <label>&nbsp;</label>
        <br>
        <button type="submit" class="btn btn-primary">Сформировать</button>
    </div>
</form>