
<?php
isset($ds) ? $date_start=$ds : $date_start = date("Y-m-01");
isset($de) ? $date_end=$de : $date_end = date("Y-m-d");
?>

<h1>
    Формирование графика приема граждан руководителями ОИВ
</h1>

<p>Введите диапазон дат формирования отчета (Год-Месяц-День):</p>
<form method="post" action="" class="form-horizontal">
    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
    <div class="col-xs-4">
        <label for="date_start">Дата начала</label>
        <input type="date" name="date_start" value="<?= $date_start; ?>" class="form-control" id="date_start">
    </div>
    <div class="col-xs-4">
        <label for="date_end">Дата окончания</label>
        <input type="date" name="date_end" value="<?= $date_end; ?>" class="form-control" id="date_end">
    </div>
    <div class="col-xs-4">
        <label>&nbsp;</label>
        <br>
        <button type="submit" class="btn btn-primary">Сформировать</button>
    </div>
    <br>
    <br>
    <br>
    <br>
</form>

<?php
if (isset($res[0])) {
    echo "<table class=\"table table-bordered\">";
    echo "<tr>";
    // Вывод заголовка таблицы
    foreach ($res[0] as $key1 => $row1) {
        echo "<th>" . $key1 . "</th>";
    }
    echo "</tr>";
    // Вывод содержания таблицы
    foreach ($res as $key => $row) {
        echo "<tr>";
        foreach ($row as $key2 => $row2) {
            echo "<td>" . $row2 . "</td>";
        }
        echo "</tr>";
    }
    echo "</table>";

}
?>

