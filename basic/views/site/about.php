<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'О сайте';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>


        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut convallis, lacus eu interdum pellentesque, magna neque maximus odio, et sollicitudin urna sapien ut dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean enim nibh, hendrerit a tellus vitae, aliquet pretium justo. Aenean rhoncus porttitor finibus. Pellentesque elit diam, ullamcorper quis metus vitae, commodo semper neque. Nullam commodo dolor ut justo interdum fermentum lacinia vitae dolor. Donec suscipit, ligula et finibus feugiat, ligula neque porttitor quam, non luctus massa elit et augue. Sed iaculis non tellus nec pretium. Duis euismod nunc euismod, sagittis massa ac, blandit ante. Nullam ornare, lectus a feugiat lacinia, massa nisl convallis justo, ac blandit ipsum eros eu lectus. Quisque sollicitudin risus sed dui auctor condimentum.
        </p>
        <p>
            Etiam eget tincidunt urna, non gravida quam. Sed enim arcu, eleifend sed laoreet ut, finibus id diam. Nulla vel imperdiet massa. Sed non eleifend mauris. Donec fringilla sed diam et gravida. Etiam non nulla vel ex imperdiet pharetra ut pharetra libero. Nulla facilisi. Aliquam justo leo, ultrices vel turpis et, efficitur placerat ante. Aliquam sit amet massa in tortor commodo aliquet. Integer non leo dignissim, mollis lacus quis, mattis erat. Donec rutrum condimentum felis, quis aliquam tortor viverra at. Praesent arcu risus, laoreet in metus malesuada, sollicitudin convallis tortor. Curabitur et congue nunc, id tincidunt elit. Duis tincidunt, est in luctus fermentum, massa mauris sodales quam, eget ullamcorper eros tortor eu justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
        </p>
        <p>
            Duis ac libero mi. Donec feugiat justo a nisl cursus, id fringilla arcu pulvinar. Nullam rhoncus tristique nibh et pharetra. Vestibulum porttitor pharetra scelerisque. Fusce ultrices at nunc in semper. Duis suscipit metus eu urna mattis, ut posuere tellus pharetra. Phasellus rhoncus libero vel elit volutpat, eu condimentum arcu pretium. Vivamus suscipit, nisl at interdum condimentum, augue ipsum auctor risus, at posuere est diam ac lorem. Curabitur sem orci, elementum non sagittis ac, congue ac mi. Aliquam molestie leo tempus risus dapibus imperdiet. Nulla nec ipsum lorem. Praesent ipsum enim, iaculis ac odio a, fermentum cursus lorem. Nunc dignissim nulla massa, non accumsan velit scelerisque sed. Nam vel lacinia urna. Etiam porta nibh sit amet tortor tincidunt, vel sollicitudin eros tempor.
        </p>
        <p>
            Sed consectetur leo sit amet risus tincidunt vulputate. Cras ornare, libero condimentum sollicitudin pharetra, diam quam faucibus lorem, nec gravida nulla urna id lacus. In auctor eleifend magna, ac euismod ligula laoreet quis. Sed dictum mi sed luctus vestibulum. Duis posuere dapibus gravida. Nunc eget tellus enim. Integer non arcu vestibulum, sollicitudin est quis, fringilla tellus. Vestibulum pulvinar vulputate purus at consequat. Fusce feugiat arcu ligula, vel facilisis purus semper vel. Integer rhoncus felis malesuada diam euismod, dignissim varius turpis sodales. Morbi nec tortor at ipsum posuere auctor eu quis libero. Nunc et eros sit amet diam maximus tincidunt. Suspendisse potenti. Vestibulum pharetra id mauris eget congue.
        </p>
        <p>
            Sed eget augue euismod, rhoncus felis at, consequat est. Vivamus iaculis non turpis eget maximus. Sed vel ultricies lectus, vitae convallis lorem. Nunc hendrerit efficitur dolor ut mollis. Quisque maximus velit risus. Curabitur dapibus nunc vitae ullamcorper tristique. Duis non odio eu tellus gravida viverra.
        </p>

</div>
