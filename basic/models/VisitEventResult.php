<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visit_event_result".
 *
 * @property integer $id
 * @property integer $event_id
 * @property string $nrk
 * @property integer $region_id
 * @property integer $category_id
 * @property integer $answer_id
 * @property string $date_zap
 * @property string $status
 *
 * @property VisitEvent $event
 */
class VisitEventResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visit_event_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'region_id', 'category_id', 'answer_id', 'type_id'], 'integer'],
            [['date_zap'], 'safe'],
            [['status'], 'string'],
            [['nrk'], 'string', 'max' => 255],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => VisitEvent::className(), 'targetAttribute' => ['event_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'nrk' => 'Nrk',
            'region_id' => 'Region ID',
            'type_id' => 'Type ID',
            'category_id' => 'Category ID',
            'answer_id' => 'Answer ID',
            'date_zap' => 'Date Zap',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(VisitEvent::className(), ['id' => 'event_id']);
    }
}
