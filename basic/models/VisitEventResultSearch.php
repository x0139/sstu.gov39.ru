<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VisitEventResult;

/**
 * VisitEventResultSearch represents the model behind the search form about `app\models\VisitEventResult`.
 */
class VisitEventResultSearch extends VisitEventResult
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'event_id', 'region_id', 'category_id', 'answer_id', 'type_id'], 'integer'],
            [['nrk', 'date_zap', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VisitEventResult::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'event_id' => $this->event_id,
            'region_id' => $this->region_id,
            'category_id' => $this->category_id,
            'answer_id' => $this->answer_id,
//            'type_id' => $this->type_id,
            'date_zap' => $this->date_zap,
        ]);

        $query->andFilterWhere(['like', 'nrk', $this->nrk])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
