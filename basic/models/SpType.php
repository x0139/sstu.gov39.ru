<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sp_type".
 *
 * @property integer $id
 * @property string $type_title
 * @property string $status
 */
class SpType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sp_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'string'],
            [['type_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_title' => 'Type Title',
            'status' => 'Status',
        ];
    }
}
