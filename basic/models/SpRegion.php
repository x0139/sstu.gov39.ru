<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sp_region".
 *
 * @property integer $id
 * @property string $region_title
 * @property string $status
 */
class SpRegion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sp_region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'string'],
            [['region_title'], 'string', 'max' => 255],
            [['sort_order'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'region_title' => 'Region Title',
            'status' => 'Status',
            'sort_order' => 'Порядок сортировки',
        ];
    }
}
