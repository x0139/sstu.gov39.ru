<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sp_category".
 *
 * @property integer $id
 * @property string $category_title
 * @property integer $type_id
 * @property string $status
 */
class SpCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sp_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id'], 'integer'],
            [['status'], 'string'],
            [['category_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_title' => 'Category Title',
            'type_id' => 'Type ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpType()
    {
        return $this->hasOne(SpType::className(), ['id' => 'type_id']);
    }
}
