<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sp_answer".
 *
 * @property integer $id
 * @property string $answer_title
 * @property string $enum
 */
class SpAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sp_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'string'],
            [['answer_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'answer_title' => 'Answer Title',
            'status' => 'Статус',
        ];
    }
}
