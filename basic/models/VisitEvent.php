<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visit_event".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $date_visit_start
 * @property string $date_visit_end
 * @property string $status
 * @property string $date_zap
 *
 * @property User $user
 */
class VisitEvent extends \yii\db\ActiveRecord
{
    public $startHour;
    public $startMinute;
    public $endHour;
    public $endMinute;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visit_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'date_visit_start', 'date_visit_end', 'status', 'startHour', 'startMinute', 'endHour', 'endMinute'], 'required'],
            [['startHour', 'startMinute', 'endHour', 'endMinute'], 'integer'],
            [['startHour', 'endHour'], 'compare', 'compareValue' => 24, 'operator' => '<'],
            [['startHour', 'endHour'], 'compare', 'compareValue' => 0, 'operator' => '>='],
            [['startMinute', 'endMinute'], 'compare', 'compareValue' => 60, 'operator' => '<'],
            [['startMinute', 'endMinute'], 'compare', 'compareValue' => 0, 'operator' => '>='],
            [['status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID записи',
            'user_id' => 'ID пользователя',
            'date_visit_start' => 'Дата начала приема',
            'date_visit_end' => 'Дата окончания приема',
            'status' => 'Статус записи',
            'date_zap' => 'Дата вненсения сведений',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
