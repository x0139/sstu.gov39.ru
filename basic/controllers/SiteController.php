<?php

namespace app\controllers;

use mysqli;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionVisitEvent()
    {
        return $this->render('visitEvent');
    }

    public function actionReport()
    {
        if (Yii::$app->user->isGuest) {
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            return $this->render('login', [
                'model' => $model,
            ]);
        }
        $session = Yii::$app->session;
        $user = array();
        $user['id'] = \Yii::$app->user->identity->id;
        $user['group_id'] = \Yii::$app->user->identity->group_id;
        $session['user'] = $user;
        return $this->render('report');
    }



    public function actionSchedule()
    {
        $usr=null;
        $dateStart=null;
        $dateEnd=null;

        if (Yii::$app->user->isGuest) {
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            return $this->render('login', [
                'model' => $model,
            ]);
        }

        $request = Yii::$app->request;
        if ($request->isPost) {
            $dateStart = $request->post('date_start');
            $dateEnd = $request->post('date_end');

            if (validateDate($dateStart) && validateDate($dateEnd)) {
                $text="CALL summary_report(str_to_date(:dateStart, '%Y-%m-%d %H:%i:%s'),str_to_date(:dateEnd, '%Y-%m-%d %H:%i:%s'));";
                $usr = Yii::$app->db->createCommand($text)->bindValue(':dateStart', $dateStart)->bindValue(':dateEnd', $dateEnd)->queryAll();
            }
        }

        if (sizeof($usr)==1) {
            $usr= null;
        }
        return $this->render('schedule',array('res'=>$usr,'ds'=>$dateStart,'de'=>$dateEnd));
    }
}

function validateDate($date)
{
    $d = \DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') == $date;
}
