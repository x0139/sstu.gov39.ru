<?php

namespace app\controllers;

use app\models\VisitEventResult;
use app\models\Model;
use Yii;
use app\models\VisitEvent;
use app\models\VisitEventSearch;
use yii\bootstrap\ActiveForm;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * VisitEventController implements the CRUD actions for VisitEvent model.
 */
class VisitEventController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete', 'view'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all VisitEvent models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $searchModel = new VisitEventSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        //$events = VisitEvent::find()->all();
        $events = VisitEvent::find()->where(['user_id' => Yii::$app->user->getId()])->all();

//        echo "<pre>";
//        print_r($events);
//        echo "</pre>";
        $tasks = [];
        foreach ($events as $event)
        {
            $task = new \yii2fullcalendar\models\Event();
            $task->id = $event->id;
            $timeStart = date("H:i", strtotime($event->date_visit_start));
            $timeEnd = date("H:i", strtotime($event->date_visit_end));
            $task->title = $timeStart . ' - ' . $timeEnd;
            $task->url = '/visit-event/view?id=' . $event->id;
//            $task->title = ' - ' . $timeEnd;
            $task->start = $event->date_visit_start;
            $task->end = $event->date_visit_end;
//            $task->backgroundColor = 'cyan';
            $tasks[] = $task;
        }

        return $this->render('index', [
            'events' => $tasks,
        ]);
    }

    /**
     * Displays a single VisitEvent model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
//        $model = $this->findModel($id);
//        $modelsVisitEventResult = VisitEventResult::find()->where(['event_id' => $model->id])->all();
//        return $this->render('view', [
//            'model' => $model,
//            //'modelsVisitEventResult' => (empty($modelsVisitEventResult)) ? [new VisitEventResult] : $modelsVisitEventResult
//            'modelsVisitEventResult' => (empty($modelsVisitEventResult)) ? [new VisitEventResult] : $modelsVisitEventResult
//
//        ]);

        return $this->actionUpdate($id);

    }

    /**
     * Creates a new VisitEvent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($date)
    {
        $model = new VisitEvent();
        $modelsVisitEventResult = [new VisitEventResult];
        $model->user_id = \Yii::$app->getUser()->id;
        $datetime = date('d.m.Y', strtotime($date));
        $model->date_visit_start = $datetime;
        $model->date_visit_end = $datetime;

        if ($model->load(Yii::$app->request->post()))
        {

            $model->user_id = \Yii::$app->getUser()->id;

            $dateStart = date('Y-m-d', strtotime($model->date_visit_start));
            $dateEnd = date('Y-m-d', strtotime($model->date_visit_end));
            $model->date_visit_start = $dateStart . ' ' . $model->startHour . ':' . $model->startMinute . ':00';
            $model->date_visit_end = $dateEnd . ' ' . $model->endHour . ':' . $model->endMinute . ':00';

            $model->status = 'active';
            $model->date_zap = date('Y-m-d H:i:s');
            $model->save();

            // multiple insert begin
            $modelsVisitEventResult = Model::createMultiple(VisitEventResult::classname());
            Model::loadMultiple($modelsVisitEventResult, Yii::$app->request->post());

//            // ajax validation
//            if (Yii::$app->request->isAjax) {
//                Yii::$app->response->format = Response::FORMAT_JSON;
//                return ArrayHelper::merge(
//                    ActiveForm::validateMultiple($modelsVisitEventResult),
//                    ActiveForm::validate($modelCustomer)
//                );
//            }

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsVisitEventResult) && $valid;

            if ($valid)
            {
                $transaction = \Yii::$app->db->beginTransaction();
                try
                {
                    if ($flag = $model->save(false))
                    {
                        foreach ($modelsVisitEventResult as $modelVisitEventResult)
                        {
//                            if (!$modelVisitEventResult->question_type && !$modelVisitEventResult->question_result){
//                                break;
//                            }
                            $modelVisitEventResult->event_id = $model->id;
                            $modelVisitEventResult->date_zap = date('Y-m-d H:i:s');
                            if (!($flag = $modelVisitEventResult->save(false)))
                            {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag)
                    {
                        $transaction->commit();
//                        return $this->redirect(['view', 'id' => $model->id]);
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e)
                {
                    $transaction->rollBack();
                }
            }
            // multiple insert end

            //return $this->redirect(['index']);
        } else
        {
            return $this->renderAjax('create', [
                'model' => $model,
                'modelsVisitEventResult' => (empty($modelsVisitEventResult)) ? [new VisitEventResult] : $modelsVisitEventResult
            ]);
        }
    }

    /**
     * Updates an existing VisitEvent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelsVisitEventResult = VisitEventResult::find()->where(['event_id' => $model->id])->all();

        $dateStart = $model->date_visit_start;
        $dateEnd = $model->date_visit_end;
        $model->date_visit_start = substr($dateStart, 0, 10);
        $model->startHour = substr($dateStart, 11, 2);
        $model->startMinute = substr($dateStart, 14, 2);
        $model->date_visit_end = substr($dateEnd, 0, 10);
        $model->endHour = substr($dateEnd, 11, 2);
        $model->endMinute = substr($dateEnd, 14, 2);

        if ($model->load(Yii::$app->request->post()))
        {

            $dateStart = date('Y-m-d', strtotime($model->date_visit_start));
            $dateEnd = date('Y-m-d', strtotime($model->date_visit_end));
            $model->date_visit_start = $dateStart . ' ' . $model->startHour . ':' . $model->startMinute . ':00';
            $model->date_visit_end = $dateEnd . ' ' . $model->endHour . ':' . $model->endMinute . ':00';
            $model->save();

            $oldIDs = ArrayHelper::map($modelsVisitEventResult, 'id', 'id');
            $modelsVisitEventResult = Model::createMultiple(VisitEventResult::className(), $modelsVisitEventResult);
            Model::loadMultiple($modelsVisitEventResult, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsVisitEventResult, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsVisitEventResult) && $valid;

            if ($valid)
            {
                $transaction = \Yii::$app->db->beginTransaction();
                try
                {
                    if ($flag = $model->save(false))
                    {
                        if (!empty($deletedIDs))
                        {
                            VisitEventResult::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsVisitEventResult as $modelVisitEventResult)
                        {
                            $modelVisitEventResult->event_id = $model->id;
                            $modelVisitEventResult->date_zap = date('Y-m-d H:i:s');
                            if (!($flag = $modelVisitEventResult->save(false)))
                            {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag)
                    {
                        $transaction->commit();
//                        return $this->redirect(['view', 'id' => $model->id]);
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e)
                {
                    $transaction->rollBack();
                }
            }

            //return $this->redirect(['view', 'id' => $model->id]);
        } else
        {
            return $this->render('update', [
                'model' => $model,
                'modelsVisitEventResult' => (empty($modelsVisitEventResult)) ? [new VisitEventResult] : $modelsVisitEventResult
            ]);
        }
    }

    public function actionValidation()
    {
        $model = new VisitEvent();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()))
        {
            Yii::$app->response->format = 'json';
            return ActiveForm::validate($model);
        }
    }

    /**
     * Deletes an existing VisitEvent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $modelsVisitEventResult = VisitEventResult::find()->where(['event_id' => $model->id])->all();

        foreach($modelsVisitEventResult as $modelEvent){
            $modelEvent->delete();
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VisitEvent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VisitEvent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VisitEvent::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
